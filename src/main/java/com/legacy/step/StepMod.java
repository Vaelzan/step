package com.legacy.step;

import java.util.logging.Logger;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

@Mod(StepMod.MODID)
public class StepMod
{
	public static final String NAME = "Step";
	public static final String MODID = "step";
	public static Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public StepMod()
	{
		MinecraftForge.EVENT_BUS.register(new StepEntityEvents());
	}
}
